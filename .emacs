;POUR CAML (tuareg )
;;c chez moi pour l'instant
(setq load-path (cons "tuareg-mode" load-path)) 

(setq auto-mode-alist (cons '("\\.ml\\w?" . tuareg-mode) auto-mode-alist))
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
(autoload 'camldebug "camldebug" "Run the Caml debugger" t)

(if (and (boundp 'window-system) window-system)
    (when (string-match "XEmacs" emacs-version)
        (if (not (and (boundp 'mule-x-win-initted) mule-x-win-initted))
            (require 'sym-lock))
        (require 'font-lock)))

;; pour pouvoir taper F1 au lieu de C-x C-e pour executer la commande courrante
(global-set-key [f1] ' tuareg-eval-phrase)

(global-font-lock-mode 1)                ; syntax highlighting always on..

;;special char
(set-keyboard-coding-system 'iso-latin-1); enable input of special characters
;set-terminal-coding-system 'iso-8859-15-unix)
