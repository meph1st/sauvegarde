#   _________  _   _ ____   ____ 
#  |__  / ___|| | | |  _ \ / ___|
#    / /\___ \| |_| | |_) | |    
# _ / /_ ___) |  _  |  _ <| |___ 
#(_)____|____/|_| |_|_| \_\\____|
#


########################################################################
#############                   PROMPT                    ##############
########################################################################


function precmd {

    local TERMWIDTH
    (( TERMWIDTH = ${COLUMNS} - 1 ))


    ###
    # Truncate the path if it's too long.
    
    PR_FILLBAR=""
    PR_PWDLEN=""
    
    local promptsize=${#${(%):---(%n@%m-------)---()--}}
    local pwdsize=${#${(%):-%~}}
    
    if [[ "$promptsize + $pwdsize" -gt $TERMWIDTH ]]; then
        ((PR_PWDLEN=$TERMWIDTH - $promptsize))
    else
    PR_FILLBAR="\${(l.(($TERMWIDTH - ($promptsize + $pwdsize)))..${PR_HBAR}.)}"
    fi


    ###
    # Get APM info.

    if which ibam > /dev/null; then
    PR_APM_RESULT=`ibam --percentbattery`
    elif which apm > /dev/null; then
    PR_APM_RESULT=`apm`
    fi
}


setopt extended_glob
preexec () {
    if [[ "$TERM" == "screen" ]]; then
    local CMD=${1[(wr)^(*=*|sudo|-*)]}
    echo -n "\ek$CMD\e\\"
    fi
}


setprompt () {
    ###
    # Need this so the prompt will work.

    setopt prompt_subst


    ###
    # See if we can use colors.

    autoload colors zsh/terminfo
    #if [[ "$terminfo[colors]" -ge 8 ]]; then
    #colors
    #fi
    #for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
    #eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
    #eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
    #(( count = $count + 1 ))
    #done
    PR_NO_COLOUR="%{$terminfo[sgr0]%}"


    ###
    # See if we can use extended characters to look nicer.
    
    typeset -A altchar
    set -A altchar ${(s..)terminfo[acsc]}
    PR_SET_CHARSET="%{$terminfo[enacs]%}"
    PR_SHIFT_IN="%{$terminfo[smacs]%}"
    PR_SHIFT_OUT="%{$terminfo[rmacs]%}"
    PR_HBAR=${altchar[q]:--}
    PR_ULCORNER=${altchar[l]:--}
    PR_LLCORNER=${altchar[m]:--}
    PR_LRCORNER=${altchar[j]:--}
    PR_URCORNER=${altchar[k]:--}

    
    ###
    # Decide if we need to set titlebar text.
    
    case $TERM in
    xterm*)
        PR_TITLEBAR=$'%{\e]0;%(!.-=*[ROOT]*=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\a%}'
        ;;
    screen)
        PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\e\\%}'
        ;;
    *)
        PR_TITLEBAR=''
        ;;
    esac
    
    
    ###
    # Decide whether to set a screen title
    if [[ "$TERM" == "screen" ]]; then
    PR_STITLE=$'%{\ekzsh\e\\%}'
    else
    PR_STITLE=''
    fi
    
    
    ###
    # APM detection
    
    if which ibam > /dev/null; then
    PR_APM='$PR_RED${${PR_APM_RESULT[(f)1]}[(w)-2]}%%(${${PR_APM_RESULT[(f)3]}[(w)-1]})$PR_LIGHT_BLUE:'
    elif which apm > /dev/null; then
    PR_APM='$PR_RED${PR_APM_RESULT[(w)5,(w)6]/\% /%%}$PR_LIGHT_BLUE:'
    else
    PR_APM=''
    fi
    
    
    ###
    # Finally, the prompt.

    PROMPT='%B$PR_SET_CHARSET$PR_STITLE${(e)PR_TITLEBAR}\
$PR_CYAN$PR_SHIFT_IN$PR_ULCORNER$PR_BLUE$PR_HBAR$PR_SHIFT_OUT(\
$PR_GREEN%(!.%SROOT%s.%n)$PR_GREEN@%m\
$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_CYAN$PR_HBAR${(e)PR_FILLBAR}$PR_BLUE$PR_HBAR$PR_SHIFT_OUT(\
$PR_MAGENTA%D{%d-%m-%y}\
$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_CYAN$PR_URCORNER$PR_SHIFT_OUT\

$PR_CYAN$PR_SHIFT_IN$PR_LLCORNER$PR_BLUE$PR_HBAR$PR_SHIFT_OUT(\
%(?..$PR_LIGHT_RED%?$PR_BLUE:)\
${(e)PR_APM}$PR_YELLOW%d\
$PR_LIGHT_BLUE%(!.$PR_RED.$PR_WHITE)$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT\
$PR_CYAN$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT\
$PR_NO_COLOUR>%b %#  '

    RPROMPT='%B$PR_CYAN$PR_SHIFT_IN$PR_HBAR$PR_BLUE$PR_HBAR$PR_SHIFT_OUT\
($PR_YELLOW%D{%H:%M:%S}$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_CYAN$PR_LRCORNER$PR_SHIFT_OUT$PR_NO_COLOUR%b'

    PS2='$PR_CYAN$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT\
$PR_BLUE$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT(\
$PR_LIGHT_GREEN%_$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT\
$PR_CYAN$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT$PR_NO_COLOUR'
}

setprompt

########################################################################
########################################################################
########################################################################

## Simple Prompt
#PROMPT='┌┼──(%B%n%b on %B%m%b)
#└┼──▶(in %B%d%b) %# '
#RPROMPT='[%D{%a %b - %h:%m}]'

## If not running interactively, do nothing 
[ -z "$PS1" ] && return

# Search path for the cd command
cdpath=(.. ~ ~/src ~/zsh)

# --// History //--
export HISTFILE=~/.zsh_history
export HISTSIZE=2000
export SAVEHIST=2000
eval `dircolors -b`
setopt EXTENDED_HISTORY        # puts timestamps in the history
setopt HIST_VERIFY        # when using ! cmds, confirm first
setopt HIST_IGNORE_DUPS        # ignore same commands run twice+
setopt APPEND_HISTORY        # don't overwrite history 
setopt SHARE_HISTORY        # _all_ zsh sessions share the same history files
setopt INC_APPEND_HISTORY    # write after each command
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_SPACE

## load zsh modules
autoload -U zfinit
zfinit
autoload -U compinit promptinit zcalc zsh-mime-setup
compinit
promptinit
zsh-mime-setup
autoload -U compinit compinit

## Options
setopt printexitvalue          # alert me if something's failed
setopt PUSHD_MINUS
setopt NO_HUP
setopt NO_BEEP
setopt NO_CASE_GLOB
setopt IGNORE_EOF
setopt ALL_EXPORT
setopt notify globdots correct pushdtohome cdablevars autolist
setopt correctall autocd recexact longlistjobs
setopt autoresume histignoredups pushdsilent noclobber
setopt autopushd pushdminus autolist
unsetopt bgnice autoparamslash extendedglob
unsetopt ALL_EXPORT
setopt autopushd pushdminus pushdsilent pushdtohome
setopt autocd
setopt cdablevars
setopt ignoreeof
setopt interactivecomments
setopt nobanghist
setopt SH_WORD_SPLIT
setopt nohup

# Autoload zsh modules when they are referenced
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
zmodload -ap zsh/mapfile mapfile

# --// PATH and other variables //--
export PATH="/home/kooka/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games"
export EDITOR="gedit"
export BROWSER="chromium-browser"
export PAGER="most"

## Completion Styles ##
# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate
# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/3 )) numeric )'
# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions
# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''
# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters
zstyle '*' hosts $hosts
# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
    '*?.old' '*?.pro'
# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'
# allow approximate
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric
# tab completion for PID :D
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
# cd not select parent dir
zstyle ':completion:*:cd:*' ignore-parents parent pwd
# use cache
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
# others stuffs
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*: default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zstyle ':completion:*:rm:*' ignore-line yes
zstyle ':completion:*:mv:*' ignore-line yes
zstyle ':completion:*:cp:*' ignore-line yes
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
                           /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin

# --// Command not found //--
if [ -f /etc/zsh_command_not_found ]; then
    . /etc/zsh_command_not_found
fi

# Fonction "Today" :
function today {
    echo -n "Today's date is: "
    date +"%A, %B %-d, %Y"
}

# --// Aliases //--alias reload='source ~/.zshrc'
alias biggest='du -xm --max-depth=1 | sort -nr | head -10'
alias rm='rm -rv'
alias rem='rm -vi'
alias rev= 'pacman -Syu'
alias copy='cp -f'
alias cp='cp -Rv'
alias mv='mv -vi'
alias '..'='cd ..'
alias deconnect='openbox --exit'
alias reboot='sudo shutdown -r now'
alias eteindre='sudo shutdown -h now'
alias sl="sudo gedit /etc/apt/sources.list"
alias serv="ssh -XC serveur"
alias sr="screen -raAd"
alias srD="screen -S Dimi"
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias search="apt-cache search"
alias show="apt-cache show"
alias deps="apt-cache depends"
alias rdeps="apt-cache rdepends"
alias install='sudo aptitude install'
alias update='sudo aptitude update'
alias upgrade='sudo aptitude dist-upgrade'
alias dupgrade='sudo aptitude update && sudo aptitude dist-upgrade'
alias maj="sudo aptitude update && sudo aptitude upgrade && sudo aptitude dist-upgrade && sudo aptitude autoclean && aptitude clean && aptitude autoremove"
alias agi='sudo aptitude install'
alias agu='sudo aptitude update'
alias agg='sudo aptitude dist-upgrade'
alias agug='sudo aptitude update && sudo aptitude dist-upgrade'
alias purge='sudo aptitude purge'
alias agp='sudo aptitude purge'
alias agar='sudo aptitude autoremove'
alias add='sudo add-apt-repository'
alias wifi='wicd-curses'
alias son='alsamixer'
alias topten='du -sk $(/bin/ls -A) | sort -rn | head -10'  
# les 10 plus gros fichiers/répertoires du réperoitre en cours

# --// Pipes //--
alias -g G='| grep'
alias -g S='| sort'
alias -g L='| less'
alias -g T='| tail'
alias -g H='| head'

# --// General Config //--
config () {
    case $1 in
        zsh)        $EDITOR ~/.zshrc ;;
        xdefs)      $EDITOR ~/.Xdefaults ;;
        xinit)      $EDITOR ~/.xinitrc ;;
        mpd)        $EDITOR ~/.mpdconf ;;
        ncmpcpp)    $EDITOR ~/.ncmpcpp/config ;;
        fstab)      sudo $EDITOR /etc/fstab ;;
        sources)    sudo $EDITOR /etc/apt/sources.list ;;
        vim)        $EDITOR ~/.vimrc ;;
        inittab)    sudo $EDITOR /etc/inittab ;;
        tint2)      $EDITOR ~/.config/tint2/tint2rc ;;
        xorg)        sudo $EDITOR /etc/X11/xorg.conf ;;
        *)          if [ -f "$1" ]; then 
                        $EDITOR $1 
                    else 
                        echo "Invalid Option" 
                    fi ;;
    esac
}

# --// Cleanup //--
cleanup () {
    echo -e "$red * Cleaning Thumbnails * $nc"
    rm -rfv ~/.thumbnails/*
    echo -e "$red * Removing Chromium cache *$nc"
    rm -rfv ~/.cache/chromium/*
    echo -e "$red * Removing Opera cache *$nc"
    rm -rfv ~/.opera/cache/*
    echo -e "$red * Removing Flash Player cache *$nc"
    rm -rfv ~/.adobe/Flash_Player/*
    rm -rfv ~/.macromedia/Flash_Player/*
    echo -e "$red * Cleaning Trash *$nc"
    rm -rfv ~/.local/share/Trash/*
    rm -rfv /media/kooka/.Thrash-1000/*
    echo
}

chpwd() {
  [[ -o interactive ]] || return
  case $TERM in
    sun-cmd) print -Pn "\e]l%~\e\\"
      ;;
    *xterm*|rxvt|(dt|k|E)term) print -Pn "\e]2;%~\a"
      ;;
  esac
}
